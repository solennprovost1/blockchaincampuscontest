import uuid
import json


class Wallet:
    unique_id = ""
    balance = 0
    history = []

    def __init__(self, balance, uuidArray, filename):
        if filename != '':
            self.load(filename)
        else:
            self.generate_unique_id(uuidArray)
            self.balance = balance
            self.save()

    def generate_unique_id(self, uuidArray):
        unique_id = uuid.uuid1()
        if unique_id in uuidArray:
            self.generate_unique_id(uuidArray)
        else:
            self.unique_id = unique_id

    def add_balance(self, amountToAddBalance):
        self.balance += amountToAddBalance
        # self.history.append([self.unique_id, amountToAddBalance])
        self.save()

    def sub_balance(self, amountToSubBalance):
        sufficientBalance = True
        if self.balance - amountToSubBalance < 0:
            sufficientBalance = False
        else:
            self.balance -= amountToSubBalance
            # self.history.append([self.unique_id, amountToSubBalance])
            # rewrite json file
            self.save()
        return sufficientBalance

    def save(self):
        filename = str(self.unique_id) + '.json'
        # file = open('../content/wallets/' + filename, "w")
        data = {'wallet': []}
        data['wallet'].append({
            'unique_id': str(self.unique_id),
            'balance': str(self.balance),
            'history': self.history
        })
        with open('../content/wallets/' + filename, "w") as file:
            json.dump(data, file, indent=4)

    def load(self, filename):
        fileJson = open('../content/wallets/'+filename, 'r', encoding="utf8")
        # get value for initialise id, balance and transactions
        with fileJson as file:
            data = json.load(file)
            self.unique_id = data.get('unique_id')
            self.balance = data.get('balance')
        return data.get('unique_id')

# test
uuidArray = []
wallet1 = Wallet(14, uuidArray, '')
print(wallet1.unique_id)
uuidArray.append(wallet1.unique_id)
print(uuidArray)
wallet1.sub_balance(12)
wallet1.add_balance(100)
print(wallet1.load('c231a2e0-e0ac-11eb-816c-040e3c8ad5d3.json'))
