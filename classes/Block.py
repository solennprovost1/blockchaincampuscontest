import hashlib
import json
import os.path
from datetime import time


class Block:
    unique_id = 0
    size = 0
    hash = ""
    parent_hash = ""
    transactions = []

    def __init__(self, index, base_hash, parent_hash, transactions):
        self.parent_hash = parent_hash
        self.base_hash = base_hash
        self.index = index
        self.transactions = transactions
        self.timestamp = time()
        self.generate_hash()
        if not self.generate_hash():
            self.parent_hash = "0"
            self.index = ""
            self.base_hash = 0
            self.transactions = []
        else:
            self.save()

    def generate_hash(self):
        blockString = "{}{}{}{}{}".format(self.index, self.base_hash, self.parent_hash, self.transactions,
                                          self.timestamp)
        self.hash = "0000"+hashlib.sha256(blockString.encode()).hexdigest()
        hashCorrect = True
        # if not self.check_hash(listHashParent):
        #   hashCorrect = False
        return hashCorrect

    def check_hash(self, listHashParent):
        hashNotExist = True
        if self.hash in listHashParent:
            self.generate_hash()
            hashNotExist = False
        return self.hash.startswith('0000') and hashNotExist

    def add_transaction(self, transmitter, receiver, amount, stateTransaction, uuidArray):
        transmitterOk = False
        receiverOk = False
        sizeOk = False
        if transmitter in uuidArray:
            transmitterOk = True
        if transmitter in uuidArray:
            receiverOk = True
        # if self.get_weight() < 2560000:
        #     sizeOk = True
        # else:
        #     newBlock = Block(self.index + 1, [], self.hash, [], time())
        #     newBlock.add_transaction(transmitter, receiver, amount, stateTransaction, uuidArray)
        validTransaction = False
        if stateTransaction and transmitterOk and receiverOk:
            self.transactions.append({transmitter, receiver, amount})
            validTransaction = True
        return validTransaction

    def get_transaction(self, numTransactions):
        pass

    def get_weight(self):
        self.size = os.path.getsize('../content/blocs/' + self.hash + '.json')

    def save(self):
        filename = str(self.hash) + '.json'
        data = {'block': []}
        data['block'].append({
            'hash': str(self.hash),
            'parent_hash': str(self.parent_hash),
            'transactions': self.transactions
        })
        with open('../content/blocs/' + filename, "w") as file:
            json.dump(data, file, indent=4)

    def load(self, filename):
        fileJson = open('../content/blocs/' + filename, 'r', encoding="utf8")
        # get value for initialise id, balance and transactions
        with fileJson as file:
            data = json.load(file)
            self.parent_hash = data.get('parent_hash')
            self.index = data.get('index')
            self.base_hash = data.get('base_hash')
            self.transactions = data.get('transactions')
            self.hash = data.get('hash')
        return data.get('hash')
