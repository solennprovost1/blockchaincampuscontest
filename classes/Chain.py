import hashlib
import time
import Block
import Wallet


class Chain:
    blocs = []
    last_transaction_number = ""

    def __init__(self, blocs, last_transaction):
        if blocs != '':
            self.blocs.append(blocs)
        else:
            self.blocs = []
        if last_transaction != '':
            self.last_transaction_number = last_transaction
        else:
            self.last_transaction_number = ''
        self.timestamp = time.time()

    def generate_hash(self):
        self.blocs[-1].generate_hash()

    def verify_hash(self):
        self.blocs[-1].check_hash()

    def add_block(self):
        if len(self.blocs) == 0:
            firstBlock = Block.Block(0, [], time.time(), "0")
            self.blocs.append(firstBlock)
            return firstBlock
        else:
            block = Block.Block(len(self.blocs), len(self.blocs) + 1, "", [])
            self.blocs.append(block)

    def get_block(self, hash):
        pass

    def add_transaction(self, Block, Wallet1, Wallet2, amount):
        Block.add_transaction(Block, Wallet1.unique_id, Wallet2.unique_id, amount)
        if Wallet1.sub_balance(amount):
            Wallet2.add_balance(amount)
            print('Transaction accpted')
        else:
            print('Balance insufficient from' + Wallet1.unique_id)

    def find_transaction(self, numTransaction, Block):
        Block.get_transaction(numTransaction)

    def get_last_transaction_number(self):

        lastBlocs = self.blocs[-1]
        # récupérer ensuite dernière transaction
        for self.last_transaction_number in lastBlocs:
            return " "